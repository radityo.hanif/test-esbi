<div class="content">
    <div class="container bg-white shadow-lg px-5 rounded">
        <div class="row d-flex align-items-center">
            <div class="col-lg-6 text-center">
                <img src="Assets/img/man.png" width="400px" class="img-fluid" />
            </div>
            <div class="col-lg-6">
                <h1 class="mb-5 text-center">Sign In</h1>
                <div id="alert" class="alert d-none" role="alert">
                    A simple primary alert—check it out!
                </div>
                <div class="form-floating mb-3">
                    <input type="email" class="form-control" id="email">
                    <label for="email">Email</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="password" class="form-control" id="password">
                    <label for="password">Password</label>
                </div>
                <button onclick="login()" class="btn btn-primary">
                    Login
                </button>
                <div class="mt-3">
                    <a href="register">Belum mempunyai akun? daftar disini</a>
                </div>
            </div>
        </div>
    </div>
</div>