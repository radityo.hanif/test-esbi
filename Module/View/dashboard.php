<div class="content">
    <div class="container bg-white shadow-lg px-5 rounded">
        <div class="row d-flex align-items-center">
            <div class="col-12 p-5">
                <img src="https://media.licdn.com/dms/image/C4D0BAQGuGpGoZRmuNg/company-logo_200_200/0/1654750097919/esbiindonesia_logo?e=2147483647&v=beta&t=yrRHgSFnQvo6eJctlCa6vnP8ON0oWpav2bqvTZSUBZY">
                <h1>Selamat Datang <b class="text-success"><?= $fullname ?></b> di ESBI Indonesia</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam neque eaque dolorum assumenda, fuga facilis et asperiores quasi, dolorem modi magnam deleniti. Aut voluptates sit tenetur et, deleniti minima itaque.</p>
                <button onclick="logout()" class="btn btn-primary mt-4 d-flex">
                    Logout
                </button>
            </div>
        </div>
    </div>
</div>