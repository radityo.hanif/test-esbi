<div class="content">
    <div class="container bg-white shadow-lg px-5 rounded">
        <div class="row d-flex align-items-center">
            <div class="col-lg-6 text-center">
                <img src="Assets/img/man.png" width="400px" class="img-fluid" />
            </div>
            <div class="col-lg-6">
                <h1 class="mb-5 text-center">Sign Up</h1>
                <div id="alert" class="alert d-none" role="alert">
                    A simple primary alert—check it out!
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="fullname">
                    <label for="fullname">Fullname</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="email" class="form-control" id="email">
                    <label for="email">Email</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="password" class="form-control" id="password">
                    <label for="password">Password</label>
                </div>
                <button onclick="register()" class="btn btn-primary">
                    Sign Up
                </button>
                <div class="mt-3">
                    <a href="login">Sudah mempunyai akun? login disini</a>
                </div>
            </div>
        </div>
    </div>
</div>