<?php

require_once "./Core/Database.php";

class Model
{
    private $db;
    public function __construct()
    {
        $this->db = new Database();
    }

    public function create($fullname, $email, $password)
    {
        // validasi email
        $query = "SELECT * FROM users WHERE email = '$email'";
        $data = $this->db->runQuery($query);
        if ($data) {
            if (count($data)) {
                return Util::json([
                    'status_code' => 400,
                    'message' => "Maaf email $email sudah terdaftar silahkan gunakan email lainnya"
                ]);
            }
        }

        // insert data baru
        $query = "INSERT INTO users (fullname, email, password) VALUES ('$fullname', '$email', '$password')";
        $data = $this->db->runQuery($query);
        return true;
    }

    public function login($email, $password)
    {
        // validasi email
        $query = "SELECT * FROM users WHERE email = '$email' AND password = '$password'";
        $data = $this->db->runQuery($query);
        if (!$data) {
            if (!count($data)) {
                return Util::json([
                    'status_code' => 400,
                    'message' => "Login gagal silahkan cek kembali email dan password anda"
                ]);
            }
        }
        return $data[0];
    }

    public function read()
    {
        $data = $this->db->runQuery("SELECT * FROM users;");
        return $data;
    }
}
