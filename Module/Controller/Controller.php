<?php

require_once "./Module/Model/Model.php";

class Controller
{
    public $model;

    public function __construct()
    {
        $this->model = new Model();
    }

    public static function view($target)
    {
        return "./Module/View/" . $target;
    }

    public function invoke()
    {
        $path = $_SERVER['REQUEST_URI'];
        $path = str_replace("/test-esbi", "", $path);

        switch ($path) {
            case '/login':
                $this->page_login();
                break;
            case '/register':
                $this->page_register();
                break;
            case '/dashboard':
                $this->page_dashboard();
                break;
            case '/api/register':
                $this->api_register();
                break;
            case '/api/login':
                $this->api_login();
                break;
            default:
                $this->page_register();
                break;
        }
    }

    public function page_login()
    {
        include self::view("Layout/head.php");
        include self::view("login.php");
        include self::view("Layout/foot.php");
    }

    public function page_register()
    {
        include self::view("Layout/head.php");
        include self::view("register.php");
        include self::view("Layout/foot.php");
    }

    public function page_dashboard()
    {
        $fullname = false;
        if (isset($_SESSION["user"])) {
            if (isset($_SESSION["user"]["fullname"])) {
                $fullname = $_SESSION["user"]["fullname"];
            }
        }
        if (!$fullname) {
            return $this->page_login();
        }
        include self::view("Layout/head.php");
        include self::view("dashboard.php");
        include self::view("Layout/foot.php");
    }

    public function api_register()
    {
        $result = $this->model->create($_POST['fullname'], $_POST['email'], $_POST['password']);
        if ($result) {
            return Util::json([
                "result" => $result,
                "message" => "berhasil membuat akun baru",
                "status_code" => 200
            ]);
        }
    }

    public function api_login()
    {
        $result = $this->model->login($_POST['email'], $_POST['password']);
        if ($result) {
            $_SESSION["user"] = $result;
            return Util::json([
                "message" => "berhasil login ke aplikasi",
                "status_code" => 200
            ]);
        }
    }
}
