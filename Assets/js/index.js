function showAlert({ message = "", isError = true }) {
  const selector = "#alert";
  $(selector).removeClass("d-none");
  if (isError) {
    $(selector).addClass("alert-danger");
  } else {
    $(selector).addClass("alert-success");
  }
  $(selector).html(message);
}

function initAlert() {
  const errorMessage = localStorage.getItem("error_message");
  const successMessage = localStorage.getItem("success_message");
  console.log({ errorMessage, successMessage });
  if (errorMessage != null) {
    showAlert({
      message: errorMessage,
      isError: true,
    });
  } else if (successMessage != null) {
    showAlert({
      message: successMessage,
      isError: false,
    });
  }
  localStorage.clear();
}

function register() {
  const data = {
    fullname: $("#fullname").val(),
    email: $("#email").val(),
    password: $("#password").val(),
  };

  // Validate if any property in data is an empty string
  for (const key in data) {
    if (data[key].trim() === "") {
      showAlert({
        message: "Seluruh form wajib diisi",
        isError: true,
      });
      return;
    }
  }

  $.ajax({
    type: "POST",
    url: "api/register",
    data: data,
    success: function (response) {
      localStorage.setItem("success_message", "Berhasil membuat akun baru");
      window.location.href = "login";
    },
    error: function (response) {
      const data = response.responseJSON;
      showAlert({
        message: data?.message ? data?.message : "Maaf terjadi kesalahan pada aplikasi",
        isError: true,
      });
    },
  });
}

function login() {
  const data = {
    email: $("#email").val(),
    password: $("#password").val(),
  };

  // Validate if any property in data is an empty string
  for (const key in data) {
    if (data[key].trim() === "") {
      showAlert({
        message: "Seluruh form wajib diisi",
        isError: true,
      });
      return;
    }
  }

  $.ajax({
    type: "POST",
    url: "api/login",
    data: data,
    success: function (response) {
      window.location.href = "dashboard";
    },
    error: function (response) {
      const data = response.responseJSON;
      showAlert({
        message: data?.message ? data?.message : "Maaf terjadi kesalahan pada aplikasi",
        isError: true,
      });
    },
  });
}

function logout() {
  window.location.href = "login";
}

$(document).ready(function () {
  initAlert();
});
