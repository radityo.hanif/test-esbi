<?php

session_start();

require_once './Core/Util.php';
require_once './Module/Controller/Controller.php';

// Buat objek controller
$controller = new Controller();

// Panggil method untuk menampilkan view
$controller->invoke();
