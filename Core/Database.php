<?php

class Database
{
    private $dsn;
    private $username;
    private $password;
    private $dbh;

    public function __construct($host = "localhost", $dbname = "test_oshs", $username = "root", $password = "")
    {
        $this->dsn = "mysql:host=$host;dbname=$dbname";
        $this->username = $username;
        $this->password = $password;

        try {
            $this->dbh = new PDO($this->dsn, $this->username, $this->password);
        } catch (PDOException $e) {
            die("Koneksi gagal: " . $e->getMessage());
        }
    }

    public function runQuery($query, $params = [])
    {
        $stmt = $this->dbh->prepare($query);
        $stmt->execute($params);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
