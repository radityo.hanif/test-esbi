<?php
class Util
{
    public static function json($data)
    {
        $status_code = isset($data["status_code"]) ? $data["status_code"] : 200;
        http_response_code($status_code);
        header('Content-Type: application/json');
        echo json_encode($data);
        die();
    }
}
